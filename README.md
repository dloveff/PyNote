#PyNote
使用PYTHON QT4开发的便笺小工具(高仿Windows7 StikyNot)
平台：Ubuntu14.04
  (开发者使用平台 理论上讲适用于存在PyQt4的 一切Linux平台 Windows平台暂不支持)
硬件：未知
开发语言：Python + Qt4.8(使用了qt4.8新添的api)

第一次使用请创建一个空的note.ini文件，或者将note.ini文件内容清空，之后再任意安装好
pyqt4的linux平台运行main_note.py即可

待解决问题：
1.Windows的便笺有一定的行距。Qt对于QTextEdit对象的行距设置不够灵活(QSS的text-height竟然不支持)。作者使用setLineHeight(qt4.8)来设置
了每个block的行间距，却无法设置复制粘贴过来的文本的行间距。
★待解决

![image](http://git.oschina.net/DrWindays/PyNote/raw/master/note.jpg)